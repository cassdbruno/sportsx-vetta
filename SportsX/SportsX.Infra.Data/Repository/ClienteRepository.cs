﻿using SportsX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Infra.Data.Context;

namespace SportsX.Infra.Data.Repository
{
    public class ClienteRepository : Repository<Cliente>, IClienteRepository
    {
        public ClienteRepository(SportsxContext context) : base(context) { }
        public IEnumerable<Cliente> GetByName(string name)
        {
            return Get(c => c.Nome.Contains(name));
        }

        public Cliente GetByEmail(string email)
        {
            return Get(c => c.Email == email).FirstOrDefault();
        }

        public Cliente GetByCpf(string cpf)
        {
            return Get(c => c.Cpf == cpf).FirstOrDefault();
        }

        public Cliente GetByCnpj(string cnpj)
        {
            return Get(c => c.Cnpj == cnpj).FirstOrDefault();
        }

    }
}

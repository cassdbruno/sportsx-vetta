﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SportsX.Domain.Entities;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Infra.Data.Context;

namespace SportsX.Infra.Data.Repository
{
    public class ClassificacaoRepository : Repository<Classificacao>, IClassificacaoRepository
    {
        public ClassificacaoRepository(SportsxContext context) : base(context) { }

        public IEnumerable<Classificacao> GetAll()
        {
            return GetAll();
        }
    }
}

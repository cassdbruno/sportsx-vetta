﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Infra.Data.Context;

namespace SportsX.Infra.Data.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected SportsxContext context;
        protected DbSet<TEntity> DbSet;

        public Repository(SportsxContext context)
        {
            this.context = context;
            DbSet = this.context.Set<TEntity>();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public TEntity GetById(int id)
        {
            return DbSet.Find(id);
        }

        public TEntity Insert(TEntity obj)
        {
            return DbSet.Add(obj).Entity;
        }

        public void Remove(int id)
        {
            DbSet.Remove(DbSet.Find(id));
        }

        public TEntity Update(TEntity obj)
        {
            var entry = context.Entry(obj);
            DbSet.Attach(obj);
            entry.State = EntityState.Modified;

            return obj;
        }

        public void Dispose()
        {
            context.Dispose();
            GC.SuppressFinalize(this);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }
    }
}

﻿using SportsX.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Infra.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SportsxContext _sportxContext;
        private bool _disposed;

        public UnitOfWork(SportsxContext sportsxContext)
        {
            _sportxContext = sportsxContext;
            _disposed = false;
        }

        public void Commit()
        {
            _sportxContext.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing) _sportxContext.Dispose();

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

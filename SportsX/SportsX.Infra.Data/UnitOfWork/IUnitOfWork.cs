﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Infra.Data.UnitOfWork
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}

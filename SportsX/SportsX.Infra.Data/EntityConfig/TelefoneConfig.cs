﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SportsX.Domain.Entities;

namespace SportsX.Infra.Data.EntityConfig
{
    /// <summary>
    /// Classe de configuração das propriedas e mapeamento de relacionamento 
    /// </summary>
    public class TelefoneConfig : IEntityTypeConfiguration<Telefone>
    {
        public void Configure(EntityTypeBuilder<Telefone> builder)
        {
            builder.HasKey(t => t.Id);

            builder.HasOne(t => t.Cliente)
                .WithMany(c => c.Telefones)
                .HasForeignKey(t => t.ClienteFk);

            builder.Property(t => t.NumeroTelefone)
                .HasColumnType("nvarchar");

            builder.ToTable("Telefones");
        }
    }
}

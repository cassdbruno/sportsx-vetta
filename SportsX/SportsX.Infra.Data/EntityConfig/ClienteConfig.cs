﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SportsX.Domain.Entities;

namespace SportsX.Infra.Data.EntityConfig
{
    /// <summary>
    /// Classe de configuração das propriedas e mapeamento de relacionamento 
    /// </summary>
    public class ClienteConfig : IEntityTypeConfiguration<Cliente>
    {
        public void Configure(EntityTypeBuilder<Cliente> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("nvarchar"); 

            builder.Property(c => c.RazaoSocial)
                .HasMaxLength(100)
                .HasColumnType("nvarchar");

            builder.Property(c => c.Cpf)
                .HasColumnName("Cpf")
                .IsRequired()
                .HasMaxLength(11)
                .HasColumnType("nvarchar");

            builder.Property(c => c.CEP)
                .HasMaxLength(8)
                .HasColumnType("nvarchar");

            builder.Property(c => c.Email)
                .HasColumnName("Email")
                .IsRequired()
                .HasColumnType("nvarchar");

            builder.Property(c => c.Cnpj)
                .HasColumnName("Cnpj")
                .IsRequired()
                .HasMaxLength(11)
                .HasColumnType("nvarchar");

            builder.ToTable("Clientes");
        }
    }
}

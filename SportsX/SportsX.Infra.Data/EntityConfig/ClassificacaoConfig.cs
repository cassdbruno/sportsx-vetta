﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SportsX.Domain.Entities;

namespace SportsX.Infra.Data.EntityConfig
{
    /// <summary>
    /// Classe de configuração das propriedas e mapeamento de relacionamento 
    /// </summary>
    public class ClassificacaoConfig : IEntityTypeConfiguration<Classificacao>
    {
        public void Configure(EntityTypeBuilder<Classificacao> builder)
        {
            builder.HasKey(c => c.Id);

            builder.HasMany(c => c.Clientes)
                .WithOne(c => c.Classificacao)
                .HasForeignKey(c => c.ClassificacaoFk)
                .IsRequired();

            builder.Property(c => c.Descricao)
                .HasColumnType("nvarchar");

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SportsX.Domain.Entities;
using SportsX.Infra.Data.EntityConfig;

namespace SportsX.Infra.Data.Context
{
    public class SportsxContext : DbContext
    {
        public SportsxContext(DbContextOptions<SportsxContext> options) : base(options) { }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Classificacao> Classificacao { get; set; }
        public DbSet<Telefone> Telefones { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
            }
        }

        protected internal virtual void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ClassificacaoConfig());
            modelBuilder.ApplyConfiguration(new ClienteConfig());
            modelBuilder.ApplyConfiguration(new TelefoneConfig());

            base.OnModelCreating(modelBuilder);
        }

    }
}

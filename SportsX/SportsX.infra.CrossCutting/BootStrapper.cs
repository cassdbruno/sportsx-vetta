﻿using SimpleInjector;
using SportsX.Application.Interfaces;
using SportsX.Application.Services;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Domain.Interfaces.Services;
using SportsX.Domain.Services;
using SportsX.Infra.Data.Context.Context;
using SportsX.Infra.Data.Context.Repository;
using SportsX.Infra.Data.Context.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsX.infra.CrossCutting
{
    public class BootStrapper
    {
        public static void Register(Container container)
        {
            container.Register<SportsXContext>(Lifestyle.Scoped);

            //Aplication
            container.Register<IClienteAppService, ClienteAppService>(Lifestyle.Scoped);
            container.Register<IClassificacaoAppService, ClassificacaoAppService>(Lifestyle.Scoped);

            //Domain
            container.Register<IClienteService, ClienteService>(Lifestyle.Scoped);
            container.Register<IClassificacaoService, ClassificacaoService>(Lifestyle.Scoped);

            //Data
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<IClienteRepository, ClienteRepository>(Lifestyle.Scoped);
            container.Register<IClassificacaoRepository, ClassificacaoRepository>(Lifestyle.Scoped);
            container.Register<ITelefoneRepository, TelefoneRepository>(Lifestyle.Scoped);

        }
    }
}

﻿using SportsX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SportsX.Domain.Validations
{
    /// <summary>
    /// Classe estática que irá ser responsável pela validações dos objetos de valor
    /// </summary>
    public static class Validations
    {
        /// <summary>
        /// Método que realiza a validação do CPF. OBS.: Código de validação em http://www.macoratti.net/11/09/c_val1.htm
        /// </summary>
        /// <param name="cpf">CPF que irá ser validado</param>
        /// <returns>retona falso caso o CPF esteja incorreto e true caso o CPF informado esteja correto</returns>
        public static bool ValidarCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
            {
                return false;
            }

            var tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            }

            resto = soma % 11;
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }

            var digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            }
            resto = soma % 11;
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }

            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }

        /// <summary>
        /// Método que realiza a validação do CNPJ
        /// </summary>
        /// <param name="cnpj">CNPJ que irá ser validado</param>
        /// <returns>retona falso caso o CNPJ esteja incorreto e true caso o CNPJ informado esteja correto</returns>
        public static bool ValidarCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma = 0;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");

            if (cnpj.Length != 14)
            {
                return false;
            }

            var tempCnpj = cnpj.Substring(0, 12);

            for (int i = 0; i < 12; i++)
            {
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            }

            var resto = (soma % 11);

            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }

            var digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
            {
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            }

            resto = (soma % 11);
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }

            digito = digito + resto.ToString();

            return cnpj.EndsWith(digito);
        }

        /// <summary>
        /// Método que realiza a validação do Email. OBS.: Código de validação em http://fabiocabral.gabx.com.br/2013/03/c-funcao-para-validar-um-e-mail.html
        /// </summary>
        /// <param name="email">Endereço de e-mail que irá ser validado</param>
        /// <returns>Caso o e-mail seja válido o retorno será true, caso seja inválido o retorno será false</returns>
        public static bool ValidarEmail(string email)
        {
            bool validEmail = false;
            int indexArr = email.IndexOf("@", StringComparison.Ordinal);

            if (indexArr <= 0) return validEmail;

            if (email.IndexOf("@", indexArr + 1, StringComparison.Ordinal) > 0)
            {
                return validEmail;
            }

            int indexDot = email.IndexOf(".", indexArr, StringComparison.Ordinal);

            if (indexDot - 1 <= indexArr) return validEmail;
            if (indexDot + 1 < email.Length)
            {
                string indexDot2 = email.Substring(indexDot + 1, 1);
                if (indexDot2 != ".")
                {
                    validEmail = true;
                }
            }
            return validEmail;
        }

        public static bool ValidarCliente(Cliente cliente)
        {
            bool cpfValido = true;
            bool cnpjValido = true;
            bool emailValido = ValidarEmail(cliente.Email);

            if (!string.IsNullOrEmpty(cliente.Cpf)) cpfValido = ValidarCpf(cliente.Cpf);

            if (!string.IsNullOrEmpty(cliente.Cnpj)) cnpjValido = ValidarCnpj(cliente.Cnpj);

            return cpfValido && cnpjValido && emailValido;
        }

    }
}

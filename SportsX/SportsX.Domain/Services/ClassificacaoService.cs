﻿using System;
using System.Collections.Generic;
using System.Text;
using SportsX.Domain.Entities;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Domain.Interfaces.Services;

namespace SportsX.Domain.Services
{
    public class ClassificacaoService : IClassificacaoService
    {
        private readonly IClassificacaoRepository _classificacaoRepository;

        public ClassificacaoService(IClassificacaoRepository classificacaoRepository)
        {
            _classificacaoRepository = classificacaoRepository;
        }        

        public IEnumerable<Classificacao> GetAll()
        {
            return _classificacaoRepository.GetAll();
        }

        public Classificacao GetById(int id)
        {
            return _classificacaoRepository.GetById(id);
        }

        public void Dispose()
        {
            _classificacaoRepository.Dispose();
            GC.SuppressFinalize(this);
        }

        public Classificacao Insert(Classificacao classificacao)
        {
            return _classificacaoRepository.Insert(classificacao);
        }
    }
}

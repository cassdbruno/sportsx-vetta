﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using SportsX.Domain.Entities;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Domain.Interfaces.Services;

namespace SportsX.Domain.Services
{
    public class ClienteService : IClienteService
    {
        private readonly IClienteRepository _clienteRepository;
        private readonly ITelefoneRepository _telefoneRepository;

        public ClienteService(IClienteRepository clienteRepository, ITelefoneRepository telefoneRepository)
        {
            _clienteRepository = clienteRepository;
            _telefoneRepository = telefoneRepository;
        }

        //Método que insere o cliente caso ele for válido, caso não, retorna o cliente novamente
        public Cliente Insert(Cliente cliente)
        {
            return !cliente.EhValido() ? cliente : _clienteRepository.Insert(cliente);
        }

        //Método que irá buscar o cliente pelo id do mesmo
        public Cliente GetById(int id)
        {
            return _clienteRepository.GetById(id);
        }

        //Método que irá buscar todos os clientes
        public IEnumerable<Cliente> GetAll()
        {
            return _clienteRepository.GetAll();
        }

        //Método que irá atualizaro cliente e retornar o mesmo atualizado
        public Cliente Update(Cliente cliente)
        {
            foreach (var telefone in cliente.Telefones)
            {
                _telefoneRepository.Update(telefone);
            }
            
            return _clienteRepository.Update(cliente);
        }

        //Método que irá remover o cliente 
        public void Remove(int id)
        {
            var cliente = GetById(id);
            cliente.Telefones.Clear();

            _clienteRepository.Remove(id);
        }

        //Método que irá buscar o cliente a partir de uma expressão lambda
        public IEnumerable<Cliente> Get(Expression<Func<Cliente, bool>> predicate)
        {
            return _clienteRepository.Get(predicate);
        }
        
        public void Dispose()
        {
            _clienteRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}

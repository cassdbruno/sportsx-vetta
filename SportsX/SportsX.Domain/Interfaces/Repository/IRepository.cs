﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace SportsX.Domain.Interfaces.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        TEntity Insert(TEntity obj);
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
        TEntity Update(TEntity obj);
        void Remove(int id);
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);
        int SaveChanges();
    }
}

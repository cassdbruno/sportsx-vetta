﻿using System;
using System.Collections.Generic;
using System.Text;
using SportsX.Domain.Entities;

namespace SportsX.Domain.Interfaces.Repository
{
    public interface IClienteRepository : IRepository<Cliente>
    {
        IEnumerable<Cliente> GetByName(string name);

        Cliente GetByEmail(string email);

        Cliente GetByCpf(string cpf);

        Cliente GetByCnpj(string cnpj);

    }
}

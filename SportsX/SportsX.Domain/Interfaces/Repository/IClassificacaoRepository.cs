﻿using SportsX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Domain.Interfaces.Repository
{
    public interface IClassificacaoRepository : IRepository<Classificacao>
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using SportsX.Domain.Entities;

namespace SportsX.Domain.Interfaces.Services
{
    public interface IClienteService : IDisposable
    {
        Cliente Insert(Cliente obj);
        Cliente GetById(int id);
        IEnumerable<Cliente> GetAll();
        Cliente Update(Cliente obj);
        void Remove(int id);
        IEnumerable<Cliente> Get(Expression<Func<Cliente, bool>> predicate);
    }
}

﻿using SportsX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Domain.Interfaces.Services
{
    public interface IClassificacaoService : IDisposable
    {
        IEnumerable<Classificacao> GetAll();
        Classificacao GetById(int id);
        Classificacao Insert(Classificacao classificacao);
    }
}

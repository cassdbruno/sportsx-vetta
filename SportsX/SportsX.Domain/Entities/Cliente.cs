﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Domain.Entities
{
    public class Cliente
    {
        public Cliente()
        {
            Telefones = new List<Telefone>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string RazaoSocial { get; set; }
        public bool PessoaJuridica { get; set; }
        public string CEP { get; set; }
        public int ClassificacaoFk { get; set; }
        public virtual Classificacao Classificacao { get; set; }
        public string Cpf { get; set; }
        public string Cnpj { get; set; }
        public string Email { get; set; }
        public virtual List<Telefone> Telefones { get; set; }

        public bool EhValido()
        {
            return Validations.Validations.ValidarCliente(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Domain.Entities
{
    public class Classificacao
    {
        public Classificacao()
        {
            Clientes = new List<Cliente>();
        }

        public int Id { get; set; }
        public string Descricao { get; set; }
        public virtual ICollection<Cliente> Clientes { get; set; }
    }
}

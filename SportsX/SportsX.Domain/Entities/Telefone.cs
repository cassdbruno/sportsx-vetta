﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Domain.Entities
{
    public class Telefone
    {
        public int Id { get; set; }

        public string NumeroTelefone { get; set; }

        public int? ClienteFk { get; set; }

        public virtual Cliente Cliente { get; set; }

    }
}

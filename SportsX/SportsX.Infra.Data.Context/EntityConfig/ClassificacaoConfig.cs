﻿using SportsX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsX.Infra.Data.Context.EntityConfig
{
    public class ClassificacaoConfig : EntityTypeConfiguration<Classificacao>
    {
        public ClassificacaoConfig()
        {
            HasKey(c => c.Id);

            Property(c => c.Id)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            Property(c => c.Descricao)
                .HasColumnType("nvarchar");

            ToTable("Classificacao");
        }
    }
}

﻿using SportsX.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SportsX.Infra.Data.Context.EntityConfig
{
    public class ClienteConfig : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfig()
        {
            HasKey(c => c.Id);

            Property(c => c.Id)
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity); 

            Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("nvarchar")
                .HasMaxLength(100);

            Property(c => c.RazaoSocial)
                    .HasMaxLength(100)
                    .HasColumnType("nvarchar");

            Property(c => c.Cpf)
                    .HasColumnName("Cpf")
                    .HasMaxLength(11)
                    .HasColumnType("nvarchar");

            Property(c => c.CEP)
                    .HasMaxLength(8)
                    .HasColumnType("nvarchar");

            Property(c => c.Email)
                    .HasColumnName("Email")
                    .IsRequired()
                    .HasColumnType("nvarchar");

            Property(c => c.Cnpj)
                    .HasColumnName("Cnpj")
                    .HasMaxLength(14)
                    .HasColumnType("nvarchar");

            HasRequired(c => c.Classificacao)
                .WithMany(c => c.Clientes)
                .HasForeignKey<int>(c => c.ClassificacaoFk);                

            ToTable("Clientes");
        }
    }
}

﻿using SportsX.Infra.Data.Context.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsX.Infra.Data.Context.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SportsXContext _context;
        private bool _disposed;

        public UnitOfWork(SportsXContext context)
        {
            _context = context;
            _disposed = false;
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing) _context.Dispose();

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

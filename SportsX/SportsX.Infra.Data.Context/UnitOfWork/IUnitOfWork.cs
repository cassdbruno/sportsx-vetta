﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsX.Infra.Data.Context.UnitOfWork
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}

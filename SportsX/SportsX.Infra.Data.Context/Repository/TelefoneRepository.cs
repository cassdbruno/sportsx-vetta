﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsX.Domain.Entities;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Infra.Data.Context.Context;
using SportsX.Infra.Data.Context.Repository;

namespace SportsX.Infra.Data.Context.Repository
{
    public class TelefoneRepository : Repository<Telefone>, ITelefoneRepository
    {
        public TelefoneRepository(SportsXContext context) : base(context)
        {
        }
    }
}

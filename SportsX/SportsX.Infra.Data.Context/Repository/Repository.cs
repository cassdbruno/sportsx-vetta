﻿using SportsX.Domain.Interfaces.Repository;
using SportsX.Infra.Data.Context.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SportsX.Infra.Data.Context.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected SportsXContext context;
        protected DbSet<TEntity> DbSet;

        public Repository(SportsXContext context)
        {
            this.context = context;
            DbSet = this.context.Set<TEntity>();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public TEntity GetById(int id)
        {
            return DbSet.Find(id);
        }

        public TEntity Insert(TEntity obj)
        {
            var entity = DbSet.Add(obj);
            SaveChanges();

            return entity;
        }

        public void Remove(int id)
        {
            DbSet.Remove(DbSet.Find(id));
            SaveChanges();
        }

        public TEntity Update(TEntity obj)
        {
            var entry = context.Entry(obj);
            DbSet.Attach(obj);
            entry.State = EntityState.Modified;
            SaveChanges();

            return obj;
        }

        public void Dispose()
        {
            context.Dispose();
            GC.SuppressFinalize(this);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }
    }
}

﻿using SportsX.Domain.Entities;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Infra.Data.Context.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsX.Infra.Data.Context.Repository
{
    public class ClassificacaoRepository : Repository<Classificacao>, IClassificacaoRepository
    {
        public ClassificacaoRepository(SportsXContext context) : base(context) { }
    }
}

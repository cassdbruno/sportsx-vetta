﻿using SportsX.Domain.Entities;
using SportsX.Infra.Data.Context.EntityConfig;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace SportsX.Infra.Data.Context.Context
{
    public class SportsXContext : DbContext
    {
        public SportsXContext() : base("SportsXContext") { this.Configuration.LazyLoadingEnabled = true; }

        public DbSet<Classificacao> Classificacoes { get; set; }

        public DbSet<Cliente> Clientes { get; set; }

        public DbSet<Telefone> Telefones { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>().Property(u => u.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            modelBuilder.Configurations.Add(new ClassificacaoConfig());
            modelBuilder.Configurations.Add(new ClienteConfig());
            modelBuilder.Configurations.Add(new TelefoneConfig());

            base.OnModelCreating(modelBuilder);
        }
    }
}

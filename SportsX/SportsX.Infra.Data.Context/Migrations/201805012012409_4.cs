namespace SportsX.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clientes", "Nome", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Clientes", "Cpf", c => c.String(maxLength: 11));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clientes", "Cpf", c => c.String(nullable: false, maxLength: 11));
            AlterColumn("dbo.Clientes", "Nome", c => c.String(nullable: false, maxLength: 4000));
        }
    }
}

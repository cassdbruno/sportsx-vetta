namespace SportsX.Infra.Data.Context.Migrations
{
    using SportsX.Domain.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SportsX.Infra.Data.Context.Context.SportsXContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SportsX.Infra.Data.Context.Context.SportsXContext context)
        {

            context.Classificacoes.AddOrUpdate(new Classificacao { Id = 1, Descricao = "Ativo" });
            context.Classificacoes.AddOrUpdate(new Classificacao { Id = 2, Descricao = "Inativo"});
            context.Classificacoes.AddOrUpdate(new Classificacao { Id = 3, Descricao = "Preferencial" });

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}

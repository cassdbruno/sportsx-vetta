namespace SportsX.Infra.Data.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Classificacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 4000),
                        RazaoSocial = c.String(maxLength: 100),
                        PessoaJuridica = c.Boolean(nullable: false),
                        CEP = c.String(maxLength: 8),
                        ClassificacaoFk = c.Int(nullable: false),
                        Cpf = c.String(nullable: false, maxLength: 11),
                        Cnpj = c.String(nullable: false, maxLength: 11),
                        Email = c.String(nullable: false, maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classificacao", t => t.ClassificacaoFk, cascadeDelete: true)
                .Index(t => t.ClassificacaoFk);
            
            CreateTable(
                "dbo.Telefones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NumeroTelefone = c.String(maxLength: 4000),
                        ClienteFk = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clientes", t => t.ClienteFk)
                .Index(t => t.ClienteFk);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Telefones", "ClienteFk", "dbo.Clientes");
            DropForeignKey("dbo.Clientes", "ClassificacaoFk", "dbo.Classificacao");
            DropIndex("dbo.Telefones", new[] { "ClienteFk" });
            DropIndex("dbo.Clientes", new[] { "ClassificacaoFk" });
            DropTable("dbo.Telefones");
            DropTable("dbo.Clientes");
            DropTable("dbo.Classificacao");
        }
    }
}

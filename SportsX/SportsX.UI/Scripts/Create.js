﻿$(document).ready(function () {
    let razaoSocial = document.getElementById("ClienteViewModel_RazaoSocial");
    let cnpj = document.getElementById("ClienteViewModel_Cnpj");

    if (ClienteViewModel_PessoaJuridica.checked) {
        razaoSocial.disabled = false;
        cnpj.disabled = false;
        return;
    }

    razaoSocial.disabled = true;
    cnpj.disabled = true;

});

var countTelefone = 0;

function ehPessoaJuridica(e) {
    let razaoSocial = document.getElementById("ClienteViewModel_RazaoSocial");
    let cnpj = document.getElementById("ClienteViewModel_Cnpj");

    if (e.checked) {
        razaoSocial.disabled = false;
        cnpj.disabled = false;
        return;
    }
    razaoSocial.disabled = true;
    cnpj.disabled = true;
}

function adicionaCampoTelefone() {
    $.ajax({
        url: "AddTelefone?index=" + countTelefone,
        success: function(data) {
            $("#campos-telefone").append(data);
            countTelefone++;
        }
    });
}
﻿$(document).ready(function () {
    let razaoSocial = document.getElementById("RazaoSocial");
    let cnpj = document.getElementById("Cnpj");

    if (PessoaJuridica.checked) {
        razaoSocial.disabled = false;
        cnpj.disabled = false;
        return;
    }

    razaoSocial.value = "";
    cnpj.value = "";
    razaoSocial.disabled = true;
    cnpj.disabled = true;

});

function ehPessoaJuridica(e) {
    let razaoSocial = document.getElementById("RazaoSocial");
    let cnpj = document.getElementById("Cnpj");

    if (e.checked) {
        razaoSocial.disabled = false;
        cnpj.disabled = false;
        return;
    }

    razaoSocial.value = "";
    cnpj.value = "";
    razaoSocial.disabled = true;
    cnpj.disabled = true;
}

function adicionaCampoTelefone(countTelefone) {
    $.ajax({
        url: "AddTelefoneEdit?index=" + countTelefone,
        success: function (data) {
            $("#telefones-cliente").append(data);
            countTelefone++;
        }
    });
}
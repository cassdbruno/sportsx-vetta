﻿using SportsX.Application.Enums;
using SportsX.Application.Interfaces;
using SportsX.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SportsX.UI.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IClienteAppService _clienteAppService;
        private readonly IClassificacaoAppService _classificacaoAppService;

        public ClienteController(IClienteAppService clienteAppService, IClassificacaoAppService classificacaoAppService)
        {
            _clienteAppService = clienteAppService;
            _classificacaoAppService = classificacaoAppService;
        }

        // GET: Cliente       
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                var listaClientes = _clienteAppService.GetAll();

                var clientesFormats = Application.Utils.ViewModelToViewModel.ClienteViewModelToClienteUiList(listaClientes);

                return View(clientesFormats);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET: Cliente/Details/5       
        [HttpGet]
        public ActionResult Details(int id)
        {
            try
            {
                return View(Application.Utils.ViewModelToViewModel.ClienteViewModelToClienteUi(_clienteAppService.GetById(id)));
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        // GET: Cliente/Create
        public ActionResult Create()
        {
            ViewBag.Classificacao = new SelectList(_classificacaoAppService.GetAll(), "Id", "Descricao");

            return View();
        }

        //https://stackoverflow.com/questions/44462618/how-to-bind-listt-on-form-post-in-asp-net-mvc-c-sharp
        //https://www.google.com.br/search?q=form+to+list+asp.net+mvc&oq=form+to+list+asp.net+mvc&aqs=chrome..69i57j0l2.5502j0j4&sourceid=chrome&ie=UTF-8
        // POST: Cliente/Create
        [HttpPost]
        public ActionResult Create(ClienteTelefoneViewModel clienteTelefoneViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var clienteReturn = _clienteAppService.Insert(clienteTelefoneViewModel);
                    if (!clienteReturn.EhValido)
                        ModelState.AddModelError("", "Verifique os dados do cliente e tente novamente");
                }
                
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult AddTelefone(int index)
        {
            return PartialView(new TelefoneViewModel { Index = index });
        }

        [HttpGet]
        [Route("Edit/AddTelefoneEdit/{index}")]
        public ActionResult AddTelefoneEdit(int index)
        {
            return PartialView("AddTelefone", new TelefoneViewModel { Index = index });
        }

        // GET: Cliente/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            ViewBag.Classificacao = new SelectList(_classificacaoAppService.GetAll(), "Id", "Descricao");
            var clienteReturn = _clienteAppService.GetById(id);

            return View(clienteReturn);
        }

        // POST: Cliente/Edit/5
        [HttpPost]
        public ActionResult Edit(ClienteViewModel clienteViewModel)
        {
            try
            {
                _clienteAppService.Update(clienteViewModel);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        // GET: Cliente/Delete/5
        [HttpGet]
        public ActionResult Delete(int id)
        {
            return View(Application.Utils.ViewModelToViewModel.ClienteViewModelToClienteUi(_clienteAppService.GetById(id)));
        }

        // POST: Cliente/Delete
        [HttpPost]
        public ActionResult Delete(ClienteViewModelLista clienteViewModel)
        {
            try
            {
                _clienteAppService.Remove(clienteViewModel.Id);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult BuscarCliente()
        {
            ViewBag.Options = new SelectList(_clienteAppService.ListOptions(), "Id", "Descricao");

            return View();
        }

        [HttpPost]
        public ActionResult BuscarCliente(string opcao, string termo)
        {
            try
            {
                ViewBag.Options = new SelectList(_clienteAppService.ListOptions(), "Id", "Descricao");

                var clientes = _clienteAppService.SearchClient(opcao, termo);

                return View(clientes);
            }
            catch(Exception ex)
            {
                //Utilizar método para salvar no log do sistema
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message);
            }            
        }
    }
}

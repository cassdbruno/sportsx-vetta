﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SportsX.UI.Models
{
    public class TelefoneViewModel
    {
        [Key]
        public int Id { get; set; }
        
        public string NumeroTelefone { get; set; }

        public int Index { get; set; }
    }
}

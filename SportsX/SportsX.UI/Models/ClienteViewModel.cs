﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SportsX.UI.Models
{
    public class ClienteViewModel
    {
        public ClienteViewModel()
        {
            Telefones = new List<TelefoneViewModel>();
            Cpf = string.Empty;
            Cnpj = string.Empty;
        }

        [Key]
        public int Id { get; set; }
        [DisplayName("Nome do Cliente")]
        [Required(ErrorMessage = "Campo Nome obrigatório")]
        public string Nome { get; set; }
        [DisplayName("Pessoa Jurídica")]
        public bool PessoaJuridica { get; set; }
        [MaxLength(100, ErrorMessage = "A Razão Social deve ter no máximo 100 caracteres")]
        [DisplayName("Razão Social")]
        public string RazaoSocial { get; set; }
        [MaxLength(8, ErrorMessage = "O CEP deve ter no máximo 8 caracteres")]
        public string CEP { get; set; }
        [Required(ErrorMessage = "É necessário escolher uma classificação")]
        [DisplayName("Classificação")]
        public int ClassificacaoFk { get; set; }
        [MaxLength(11, ErrorMessage = "O CPF deve ter no máximo {1} caracteres")]
        [MinLength(11, ErrorMessage = "O CPF deve ter no mínimo {1} caracteres")]
        [DisplayName("CPF")]
        public string Cpf { get; set; }
        [MaxLength(14, ErrorMessage = "O CNPJ deve ter no máximo {1} caracteres")]
        [MinLength(14, ErrorMessage = "O CNPJ deve ter no mínimo {1} caracteres")]
        [DisplayName("CNPJ")]
        public string Cnpj { get; set; }
        [DisplayName("E-mail")]
        [EmailAddress(ErrorMessage = "Preencha um e-mail válido")]
        [Required(ErrorMessage = "O campo e-mail é obrigatório")]
        public string Email { get; set; }
        public List<TelefoneViewModel> Telefones { get; set; }

        public bool EhValido { get; set; }
    }
}

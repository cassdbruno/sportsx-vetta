﻿using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using SportsX.infra.CrossCutting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(SportsX.UI.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace SportsX.UI.App_Start
{
    public class SimpleInjectorInitializer
    {
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new SimpleInjector.Integration.Web.WebRequestLifestyle();

            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private static void InitializeContainer(Container container)
        {
            BootStrapper.Register(container);
        }
    }
}
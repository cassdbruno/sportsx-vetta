﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportsX.Application.Interfaces;
using SportsX.Application.ViewModels;

namespace SportsX.Presentation.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IClienteAppService _clienteAppService;
        private readonly IClassificacaoAppService _classificacaoAppService;
        public ClienteController(IClienteAppService clienteAppService, IClassificacaoAppService classificacaoAppService)
        {
            _clienteAppService = clienteAppService;
            _classificacaoAppService = classificacaoAppService;
        }

        [Route("listar-clientes")]
        public ActionResult Index()
        {
            ViewBag.Classificoes = _classificacaoAppService.GetAll();

            return View();
        }

        // GET: Cliente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //[Route("cadastrar-cliente")]
        public ActionResult Create()
        {
            ViewBag.Classificoes = _classificacaoAppService.GetAll();

            return View();
        }

        [Route("cadastrar-cliente")]
        [HttpPost]
        public ActionResult Create(ClienteTelefoneViewModel cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var clienteReturn = _clienteAppService.Insert(cliente);
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Cliente/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Cliente/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Cliente/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Cliente/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
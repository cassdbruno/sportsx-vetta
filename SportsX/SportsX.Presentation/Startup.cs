﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SportsX.Application.Interfaces;
using SportsX.Application.Services;
using SportsX.Domain.Entities;
using SportsX.Domain.Interfaces.Repository;
using SportsX.Domain.Interfaces.Services;
using SportsX.Domain.Services;
using SportsX.Infra.Data.Context;
using SportsX.Infra.Data.Repository;
using SportsX.Infra.Data.UnitOfWork;

namespace SportsX.Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SportsxContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("SportsXContext")));


            //Domain
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IClienteService, ClienteService>();
            services.AddScoped<IClassificacaoService, ClassificacaoService>();

            //InfraData
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<IClassificacaoRepository, ClassificacaoRepository>();
            services.AddScoped<SportsxContext>();

            //Aplication
            services.AddScoped<IClienteAppService, ClienteAppService>();
            services.AddScoped<IClassificacaoAppService, ClassificacaoAppService>();

            services.AddMvc();
            services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            //var context = app.ApplicationServices.GetService<SportsxContext>();
            //AddTestData(context);

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        //private static void AddTestData(SportsxContext sportsxContext)
        //{
        //    //Ativo, Inativo, Preferencial.

        //    var classificacao1 = new Classificacao { Descricao = "Ativo" };

        //    var classificacao2 = new Classificacao { Descricao = "Inativo" };

        //    var classificacao3 = new Classificacao { Descricao = "Preferencial" };

        //    sportsxContext.Add(classificacao1);
        //    sportsxContext.Add(classificacao2);
        //    sportsxContext.Add(classificacao3);

        //    sportsxContext.SaveChanges();
        //}
    }
}

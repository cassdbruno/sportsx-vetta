﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Application.Enums
{
    public enum EnumOptionSearch
    {
        SearchByName = 1,
        SearchByCpf = 2, 
        SearchByCnpj = 3,
        SearchByRazaoSocial = 4,
        SearchByEmail = 5
    }
}

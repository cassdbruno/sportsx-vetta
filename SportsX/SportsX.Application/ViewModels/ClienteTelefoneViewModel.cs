﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Application.ViewModels
{
    public class ClienteTelefoneViewModel
    {
        public ClienteTelefoneViewModel()
        {
            TelefonesViewModel = new List<TelefoneViewModel>()
            {
                new TelefoneViewModel()
            };
        }

        public ClienteViewModel ClienteViewModel { get; set; }
        public List<TelefoneViewModel> TelefonesViewModel { get; set; }
    }
}

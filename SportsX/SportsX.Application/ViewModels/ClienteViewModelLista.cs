﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SportsX.Application.ViewModels
{
    public class ClienteViewModelLista
    {
        public int Id { get; set; }

        [DisplayName("Nome")]
        public string Nome { get; set; }

        [DisplayName("Pessoa Jurídica")]
        public string PessoaJuridica { get; set; }

        [DisplayName("Razão Social")]
        public string RazaoSocial { get; set; }

        public string CEP { get; set; }

        [DisplayName("Classificação")]
        public string Classificacao { get; set; }

        [DisplayName("CPF / CNPJ")]
        public string CpfECnpj { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Telefones")]
        public string Telefones { get; set; }
    }
}

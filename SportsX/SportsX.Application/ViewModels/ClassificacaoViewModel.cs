﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SportsX.Application.ViewModels
{
    public class ClassificacaoViewModel
    {
        [Key]
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}

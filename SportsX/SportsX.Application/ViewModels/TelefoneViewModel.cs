﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SportsX.Application.ViewModels
{
    public class TelefoneViewModel
    {
        [Key]
        public int Id { get; set; }
        
        public string NumeroTelefone { get; set; }

        public int Index { get; set; }

        public int? ClienteFk { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SportsX.Application.Enums;
using SportsX.Application.Interfaces;
using SportsX.Application.Utils;
using SportsX.Application.ViewModels;
using SportsX.Domain.Entities;
using SportsX.Domain.Interfaces.Services;
using SportsX.Domain.Services;
using SportsX.Infra.Data.Context.UnitOfWork;

namespace SportsX.Application.Services
{
    /// <summary>
    /// Classe com os serviços da entidade de cliente na camada de aplicação
    /// </summary>
    public class ClienteAppService : AppService, IClienteAppService
    {
        private readonly IClienteService _clienteService;
        private readonly IClassificacaoAppService _classificacaoAppService;

        public ClienteAppService(IClienteService clienteService, IClassificacaoAppService classificacaoAppService, IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _clienteService = clienteService;
            _classificacaoAppService = classificacaoAppService;
        }
        
        /// <summary>
        /// Obter todos os clientes
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ClienteViewModel> GetAll()
        {
            var clientesReturn = _clienteService.GetAll();

            return clientesReturn != null ? Utils.DomainToViewModel.ClienteToClienteViewModelList(clientesReturn) : new List<ClienteViewModel>();
        }

        /// <summary>
        /// Obter cliente pelo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ClienteViewModel GetById(int id)
        {
            var cliente = _clienteService.GetById(id);
            return Utils.DomainToViewModel.ClienteToClienteViewModel(cliente);
        }

        /// <summary>
        /// obter cliente pelo Cnpj
        /// </summary>
        /// <param name="Cnpj"></param>
        /// <returns></returns>
        public ClienteViewModel GetByCnpj(string Cnpj)
        {
            var cliente = _clienteService.Get(c => c.Cnpj == Cnpj).FirstOrDefault();

            return cliente != null ? Utils.DomainToViewModel.ClienteToClienteViewModel(cliente) : null;
        }

        /// <summary>
        /// Obter o clinte pelo Cpf
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        public ClienteViewModel GetByCpf(string cpf)
        {
            var clienteReturn = _clienteService.Get(c => c.Cpf == cpf).FirstOrDefault();

            return clienteReturn != null ? Utils.DomainToViewModel.ClienteToClienteViewModel(clienteReturn) : null;
        }

        /// <summary>
        /// Obter cliente pelo Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public ClienteViewModel GetByEmail(string email)
        {
            var clienteReturn = _clienteService.Get(c => c.Email == email).FirstOrDefault();

            return clienteReturn != null ? Utils.DomainToViewModel.ClienteToClienteViewModel(clienteReturn) : null;
        }

        /// <summary>
        /// Obter clinte pelo Nome Obs.: São retornados os nomes que também começam com o termo passado 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IEnumerable<ClienteViewModel> GetByName(string name)
        {
            var clienteReturn = _clienteService.Get(c => c.Nome.StartsWith(name));

            return clienteReturn != null ? Utils.DomainToViewModel.ClienteToClienteViewModelList(clienteReturn) : null;
        }

        /// <summary>
        /// Serviço da aplicação para inserção de um novo cliente no banco
        /// </summary>
        /// <param name="clienteTelefoneViewModel"></param>
        /// <returns></returns>
        public ClienteViewModel Insert(ClienteTelefoneViewModel clienteTelefoneViewModel)
        {            
            var cliente = Utils.ViewModelToDomain.ClienteViewModelToCliente(clienteTelefoneViewModel.ClienteViewModel);
            var telefones = Utils.ViewModelToDomain.TelefoneViewModelToTelefoneList(clienteTelefoneViewModel.TelefonesViewModel);
            
            cliente.Telefones.AddRange(telefones);

            var clienteReturn = _clienteService.Insert(cliente);

            return Utils.DomainToViewModel.ClienteToClienteViewModel(clienteReturn);
        }

        /// <summary>
        /// Método de remoção de um clinte no banco
        /// </summary>
        /// <param name="id">Id do cliente</param>
        public void Remove(int id)
        {
            _clienteService.Remove(id);
        }

        /// <summary>
        /// Método que edita os dados de um cliente no banco
        /// </summary>
        /// <param name="clienteViewModel"></param>
        /// <returns></returns>
        public ClienteViewModel Update(ClienteViewModel clienteViewModel)
        {
            var cliente = Utils.ViewModelToDomain.ClienteViewModelToCliente(clienteViewModel);
            var clienteReturn = _clienteService.Update(cliente);

            return Utils.DomainToViewModel.ClienteToClienteViewModel(clienteReturn);
        }

        /// <summary>
        /// Retorna a lista de opções para a tela de pesquisa de clientes
        /// </summary>
        /// <returns></returns>
        public List<OptionSearchModel> ListOptions()
        {
            return new List<OptionSearchModel>()
            {
                new OptionSearchModel { Id = 1, Descricao = "Nome"},
                new OptionSearchModel { Id = 2, Descricao = "CPF"},
                new OptionSearchModel { Id = 3, Descricao = "CNPJ"},
                new OptionSearchModel { Id = 4, Descricao = "Razão Social"},
                new OptionSearchModel { Id = 5, Descricao = "E-mail"}
            };
        }

        /// <summary>
        /// Obter cliente pela Razão social do mesmo
        /// </summary>
        /// <param name="termo"></param>
        /// <returns></returns>
        public ClienteViewModel GetByRazaoSocial(string termo)
        {
            var clienteReturn = _clienteService.Get(c => c.RazaoSocial == termo).FirstOrDefault();

            return clienteReturn != null ? Utils.DomainToViewModel.ClienteToClienteViewModel(clienteReturn) : null;
        }

        public void Dispose()
        {
            _clienteService.Dispose();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Método que realiza a busca no banco de acordo com os critérios do usuário
        /// </summary>
        /// <param name="opcao">Opção de busca</param>
        /// <param name="termo">Termo de busca</param>
        /// <returns>Retorna uma lista de clientes encontrados</returns>
        public List<ClienteViewModelLista> SearchClient(string opcao, string termo)
        {
            var opcaoSearch = Convert.ToInt32(opcao);
            ClienteViewModel clienteViewModel;
            List<ClienteViewModelLista> clientes = new List<ClienteViewModelLista>();

            switch (opcaoSearch)
            {
                case (int)EnumOptionSearch.SearchByName:
                    var clientesReturn = GetByName(termo);
                    if (clientesReturn != null) clientes.AddRange(Application.Utils.ViewModelToViewModel.ClienteViewModelToClienteUiList(clientesReturn));
                    break;

                case (int)EnumOptionSearch.SearchByEmail:
                    clienteViewModel = GetByEmail(termo);
                    if (clienteViewModel != null) clientes.Add(Application.Utils.ViewModelToViewModel.ClienteViewModelToClienteUi(clienteViewModel));
                    break;

                case (int)EnumOptionSearch.SearchByCpf:
                    clienteViewModel = GetByCpf(termo);
                    if (clienteViewModel != null) clientes.Add(Application.Utils.ViewModelToViewModel.ClienteViewModelToClienteUi(clienteViewModel));
                    break;

                case (int)EnumOptionSearch.SearchByCnpj:
                    clienteViewModel = GetByCnpj(termo);
                    if (clienteViewModel != null) clientes.Add(Application.Utils.ViewModelToViewModel.ClienteViewModelToClienteUi(clienteViewModel));
                    break;

                case (int)EnumOptionSearch.SearchByRazaoSocial:
                    clienteViewModel = GetByRazaoSocial(termo);
                    if (clienteViewModel != null) clientes.Add(Application.Utils.ViewModelToViewModel.ClienteViewModelToClienteUi(clienteViewModel));
                    break;

                default:
                    return new List<ClienteViewModelLista>();
            }

            return clientes;
        }
    }
}

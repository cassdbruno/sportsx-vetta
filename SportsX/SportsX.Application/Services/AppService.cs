﻿using SportsX.Infra.Data.Context.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace SportsX.Application.Services
{
    public class AppService
    {
        private readonly IUnitOfWork _uow;

        public AppService(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }

        protected void Commit()
        {
            _uow.Commit();
        }
    }
}

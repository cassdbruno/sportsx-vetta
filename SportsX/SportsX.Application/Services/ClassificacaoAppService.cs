﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SportsX.Application.Interfaces;
using SportsX.Application.ViewModels;
using SportsX.Domain.Entities;
using SportsX.Domain.Interfaces.Services;
using SportsX.Infra.Data.Context.UnitOfWork;

namespace SportsX.Application.Services
{
    public class ClassificacaoAppService : AppService, IClassificacaoAppService
    {
        private readonly IClassificacaoService _classificacaoService;

        public ClassificacaoAppService(IClassificacaoService classificacaoService, IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _classificacaoService = classificacaoService;
        }

        public IEnumerable<ClassificacaoViewModel> GetAll()
        {
            var classificacoes = _classificacaoService.GetAll().ToList();

            if (!classificacoes.Any()) InsertClassificacoes(classificacoes);

            return Utils.DomainToViewModel.ClassificacaoToClassificacaoViewModel(classificacoes);
        }

        public ClassificacaoViewModel GetById(int id)
        {
            var classificacaoReturn = _classificacaoService.GetById(id);

            return classificacaoReturn != null ? Utils.DomainToViewModel.ClassificacaoToClassificacaoViewModel(classificacaoReturn) : null;
        }

        private void InsertClassificacoes(List<Classificacao> classificacoes)
        {
            classificacoes = new List<Classificacao>
            {
                new Classificacao { Id = 1, Descricao = "Ativo" },
                new Classificacao { Id = 2, Descricao = "Inativo" },
                new Classificacao { Id = 3, Descricao = "Preferencial" }
            };

            AddOrUpdate(classificacoes);
        }

        private void AddOrUpdate(List<Classificacao> classificacoes)
        {
            foreach (var classificacao in classificacoes)
            {
                _classificacaoService.Insert(classificacao);
            }
        }

        public void Dispose()
        {
            _classificacaoService.Dispose();
            GC.SuppressFinalize(this);
        }


    }
}

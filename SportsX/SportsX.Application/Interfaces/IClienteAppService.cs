﻿using System;
using System.Collections.Generic;
using System.Text;
using SportsX.Application.Utils;
using SportsX.Application.ViewModels;

namespace SportsX.Application.Interfaces
{
    public interface IClienteAppService : IDisposable
    {
        ClienteViewModel Insert(ClienteTelefoneViewModel clienteTelefoneViewModel);
        ClienteViewModel Update(ClienteViewModel clienteViewModel);
        ClienteViewModel GetByCpf(string cpf);
        ClienteViewModel GetById(int id);
        ClienteViewModel GetByCnpj(string Cnpj);
        ClienteViewModel GetByEmail(string email);
        IEnumerable<ClienteViewModel> GetByName(string name);
        IEnumerable<ClienteViewModel> GetAll();
        void Remove(int id);
        List<OptionSearchModel> ListOptions();
        ClienteViewModel GetByRazaoSocial(string termo);
        List<ClienteViewModelLista> SearchClient(string opcao, string termo);
    }
}

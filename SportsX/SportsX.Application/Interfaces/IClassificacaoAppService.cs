﻿using System;
using System.Collections.Generic;
using System.Text;
using SportsX.Application.ViewModels;

namespace SportsX.Application.Interfaces
{
    public interface IClassificacaoAppService : IDisposable
    {
        IEnumerable<ClassificacaoViewModel> GetAll();
        ClassificacaoViewModel GetById(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using SportsX.Application.ViewModels;
using SportsX.Domain.Entities;

namespace SportsX.Application.Utils
{
    public static class DomainToViewModel
    {
        /// <summary>
        /// Converte Cliente para ClienteViewModel
        /// </summary>
        /// <param name="cliente">Cliente no domain</param>
        /// <returns>ClienteViewModel</returns>
        public static ClienteViewModel ClienteToClienteViewModel(Cliente cliente)
        {
            return new ClienteViewModel
            {
                Id = cliente.Id,
                Nome = cliente.Nome,
                PessoaJuridica = cliente.PessoaJuridica,
                RazaoSocial = cliente.RazaoSocial,
                Telefones = DomainToViewModel.TelefoneViewModelToTelefoneList(cliente.Telefones),
                CEP = cliente.CEP,
                ClassificacaoFk = cliente.ClassificacaoFk,
                Cnpj = cliente.Cnpj,
                Cpf = cliente.Cpf,
                Email = cliente.Email,
                EhValido = cliente.EhValido(),
                Classificacao = cliente.Classificacao != null ? ClassificacaoToClassificacaoViewModel(cliente.Classificacao) : null

            };
        }

        /// <summary>
        /// Converte Telefone para TelefoneViewModel
        /// </summary>
        /// <param name="telefone">Telefone do domínio</param>
        /// <returns>TelefoneViewModel</returns>
        public static TelefoneViewModel TelefoneToTelefoneViewMode(Telefone telefone)
        {
            return new TelefoneViewModel
            {
                Id = telefone.Id,
                NumeroTelefone = telefone.NumeroTelefone,
                ClienteFk = telefone.ClienteFk
            };
        }

        /// <summary>
        /// Converte Classificacao para ClassificacaoViewModel
        /// </summary>
        /// <param name="classificacao">Classificacao do domínio</param>
        /// <returns>ClassificacaoViewModel</returns>
        public static ClassificacaoViewModel ClassificacaoToClassificacaoViewModel(Classificacao classificacao)
        {
            return new ClassificacaoViewModel
            {
                Id = classificacao.Id,
                Descricao = classificacao.Descricao
            };
        }

        public static List<ClienteViewModel> ClienteToClienteViewModelList(IEnumerable<Cliente> clientes)
        {
            var listClienteViewmodel = new List<ClienteViewModel>();

            foreach (var cliente in clientes)
            {
                listClienteViewmodel.Add(ClienteToClienteViewModel(cliente));
            }

            return listClienteViewmodel;
        }

        public static List<TelefoneViewModel> TelefoneViewModelToTelefoneList(List<Telefone> telefones)
        {
            var listTelefoneViewmodel = new List<TelefoneViewModel>();

            foreach (var telefone in telefones)
            {
                listTelefoneViewmodel.Add(TelefoneToTelefoneViewMode(telefone));
            }

            return listTelefoneViewmodel;
        }

        public static List<ClassificacaoViewModel> ClassificacaoToClassificacaoViewModel(IEnumerable<Classificacao> classificacoes)
        {
            var classificacoesViewModel = new List<ClassificacaoViewModel>();

            foreach (var classificacao in classificacoes)
            {
                classificacoesViewModel.Add(ClassificacaoToClassificacaoViewModel(classificacao));
            }

            return classificacoesViewModel;
        }
    }
}

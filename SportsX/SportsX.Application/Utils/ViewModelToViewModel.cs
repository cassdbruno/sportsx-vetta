﻿using SportsX.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportsX.Application.Utils
{
    public static class ViewModelToViewModel
    {
        public static ClienteViewModelLista ClienteViewModelToClienteUi(ClienteViewModel clienteViewModel)
        {
            return new ClienteViewModelLista
            {
                Id = clienteViewModel.Id,
                Nome = clienteViewModel.Nome,
                PessoaJuridica = clienteViewModel.PessoaJuridica ? "Sim" : "Não",
                RazaoSocial = clienteViewModel.RazaoSocial,
                Classificacao = clienteViewModel.Classificacao.Descricao,
                CpfECnpj = clienteViewModel.Cpf + " / " + clienteViewModel.Cnpj,
                Email = clienteViewModel.Email,
                CEP = clienteViewModel.CEP,
                Telefones = String.Join(", ", clienteViewModel.Telefones.Select(t => t.NumeroTelefone))
            };
        }

        public static IEnumerable<ClienteViewModelLista> ClienteViewModelToClienteUiList(IEnumerable<ClienteViewModel> clientesViewModel)
        {
            var clientesUi = new List<ClienteViewModelLista>();

            foreach(var cliente in clientesViewModel)
            {
                clientesUi.Add(ClienteViewModelToClienteUi(cliente));
            }

            return clientesUi;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using SportsX.Application.ViewModels;
using SportsX.Domain.Entities;

namespace SportsX.Application.Utils
{
    public class ViewModelToDomain
    {
        /// <summary>
        /// Converte ClienteViewModel para Cliente  
        /// </summary>
        /// <param name="clienteViewModel">ClienteViewModel na aplicação</param>
        /// <returns>Cliente</returns>
        public static Cliente ClienteViewModelToCliente(ClienteViewModel clienteViewModel)
        {
            return new Cliente
            {
                Id = clienteViewModel.Id,
                Nome = clienteViewModel.Nome,
                PessoaJuridica = clienteViewModel.PessoaJuridica,
                RazaoSocial = clienteViewModel.RazaoSocial,
                Telefones = TelefoneViewModelToTelefoneList(clienteViewModel.Telefones),
                CEP = clienteViewModel.CEP,
                ClassificacaoFk = clienteViewModel.ClassificacaoFk,
                Classificacao = clienteViewModel.Classificacao != null ? ClassificacaoViewModelToClassificacao(clienteViewModel.Classificacao) : null,
                Cnpj = clienteViewModel.Cnpj,
                Cpf = clienteViewModel.Cpf,
                Email = clienteViewModel.Email                
            };
        }

        /// <summary>
        /// Converte TelefoneViewModel para Telefone 
        /// </summary>
        /// <param name="telefoneViewModel">TelefoneViewModel da aplicação</param>
        /// <returns>TelefoneViewModel</returns>
        public static Telefone TelefoneViewModelToTelefone(TelefoneViewModel telefoneViewModel)
        {
            return new Telefone
            {
                Id = telefoneViewModel.Id,
                NumeroTelefone = telefoneViewModel.NumeroTelefone,
                ClienteFk = telefoneViewModel.ClienteFk
            };
        }

        /// <summary>
        /// Converte ClassificacaoViewModel para Classificacao  
        /// </summary>
        /// <param name="classificacaoViewModel">ClassificacaoViewModel da aplicação</param>
        /// <returns>ClassificacaoViewModel</returns>
        public static Classificacao ClassificacaoViewModelToClassificacao(ClassificacaoViewModel classificacaoViewModel)
        {
            return new Classificacao
            {
                Id = classificacaoViewModel.Id,
                Descricao = classificacaoViewModel.Descricao
            };
        }

        public static List<Cliente> ClienteViewModelToCliente(IEnumerable<ClienteViewModel> clientesViewModel)
        {
            var listCliente = new List<Cliente>();

            foreach (var clienteViewModel in clientesViewModel)
            {
                listCliente.Add(ClienteViewModelToCliente(clienteViewModel));
            }

            return listCliente;
        }

        public static List<Telefone> TelefoneViewModelToTelefoneList(List<TelefoneViewModel> telefonesViewModel)
        {
            var listTelefone = new List<Telefone>();

            foreach (var telefoneViewModel in telefonesViewModel)
            {
                listTelefone.Add(TelefoneViewModelToTelefone(telefoneViewModel));
            }

            return listTelefone;
        }

        public static List<Classificacao> ClassificacaoViewModelToClassificacaoList(IEnumerable<ClassificacaoViewModel> classificacoesViewModel)
        {
            var classificacoes = new List<Classificacao>();

            foreach (var classificacaoViewModel in classificacoesViewModel)
            {
                classificacoes.Add(ClassificacaoViewModelToClassificacao(classificacaoViewModel));
            }

            return classificacoes;
        }
    }
}

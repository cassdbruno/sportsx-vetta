﻿[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(SportsX.Presentation.Mvc.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace SportsX.Presentation.Mvc.App_Start
{
    using System.Reflection;
    using SimpleInjector;
    using System.Web.Mvc;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    using SportsX.infra.CrossCutting;
    using SportsX.Infra.Data.Context.Context;
    using SportsX.Application.Interfaces;
    using SportsX.Application.Services;
    using SportsX.Domain.Interfaces.Services;
    using SportsX.Domain.Services;
    using SportsX.Infra.Data.Context.UnitOfWork;
    using SportsX.Domain.Interfaces.Repository;
    using SportsX.Infra.Data.Context.Repository;
    using SportsX.Presentation.Mvc.Controllers;

    public class SimpleInjectorInitializer
    {
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<SportsXContext>(Lifestyle.Scoped);

            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private static void InitializeContainer(Container container)
        {
            BootStrapper.Register(container);
        }
    }
}
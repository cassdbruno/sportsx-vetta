﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsX.Presentation.Mvc.Models
{
    public class TelefoneViewModel
    {
        [Key]
        public int Id { get; set; }

        public string NumeroTelefone { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsX.Presentation.Mvc.Models
{
    public class ClassificacaoViewModel
    {

        [Key]
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}